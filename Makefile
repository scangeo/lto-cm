SHELL = /bin/sh
ARCH = $(shell arch)
OUTPUT = $(shell mkdir -p $(EXEC_DIR))
DESTDIR ?=
CC = gcc
EXECS = lto-cm
LARGE_FILE_FLAGS = -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64

ifeq ($(ARCH),i686)
	CFLAGS = -m32 -g -O2 -W -Wall -I ./include -D_REENTRANT $(LARGE_FILE_FLAGS)
	LDFLAGS = -m32
	LIBFILES = ./lib32/sg_lib.o ./lib32/sg_lib_data.o ./lib32/sg_io_linux.o
	EXEC_DIR = ./bin/i386
else
	CFLAGS = -g -O2 -W -Wall -I ./include -D_REENTRANT $(LARGE_FILE_FLAGS)
	LDFLAGS =
	LIBFILES = ./lib64/sg_lib.o ./lib64/sg_lib_data.o ./lib64/sg_io_linux.o
	EXEC_DIR = ./bin/x86_64
endif
$(OUTPUT)

all: lto-cm.o $(LIBFILES); $(CC) -o $(EXEC_DIR)/lto-cm $(LDFLAGS) $^

install:
	@mkdir -p $(DESTDIR)/usr/bin
	@cp $(EXEC_DIR)/lto-cm $(DESTDIR)/usr/bin
	@mkdir -p $(DESTDIR)/usr/lib32
	@mkdir -p $(DESTDIR)/usr/lib
	@mkdir -p $(DESTDIR)/usr/include
	@mkdir -p $(DESTDIR)/usr/share/man/
	@mkdir -p $(DESTDIR)/usr/share/man/en
	@mkdir -p $(DESTDIR)/usr/share/man/en/man1
	@cp ./lto-cm.1.gz $(DESTDIR)/usr/share/man/en/man1
	@cp ./include/sg_cmds.h $(DESTDIR)/usr/include
	@cp ./include/sg_cmds_basic.h $(DESTDIR)/usr/include
	@cp ./include/sg_cmds_extra.h $(DESTDIR)/usr/include
	@cp ./include/sg_cmds_mmc.h $(DESTDIR)/usr/include
	@cp ./include/sg_io_linux.h $(DESTDIR)/usr/include
	@cp ./include/sg_lib.h $(DESTDIR)/usr/include
	@cp ./include/sg_lib_data.h $(DESTDIR)/usr/include
	@cp ./include/sg_linux_inc.h $(DESTDIR)/usr/include
	@cp ./include/sg_pt.h $(DESTDIR)/usr/include
	@cp ./include/sg_pt_win32.h $(DESTDIR)/usr/include
	@cp ./lib32/sg_lib.o $(DESTDIR)/usr/lib32
	@cp ./lib32/sg_io_linux.o $(DESTDIR)/usr/lib32
	@cp ./lib32/sg_lib_data.o $(DESTDIR)/usr/lib32
	@cp ./lib64/sg_lib.o $(DESTDIR)/usr/lib
	@cp ./lib64/sg_io_linux.o $(DESTDIR)/usr/lib
	@cp ./lib64/sg_lib_data.o $(DESTDIR)/usr/lib

uninstall:
	@rm $(DESTDIR)/usr/bin/lto-cm
	@rm $(DESTDIR)/usr/share/man/en/man1/lto-cm.1.gz
	@rm $(DESTDIR)/usr/include/sg_cmds.h
	@rm $(DESTDIR)/usr/include/sg_cmds_basic.h
	@rm $(DESTDIR)/usr/include/sg_cmds_extra.h
	@rm $(DESTDIR)/usr/include/sg_cmds_mmc.h
	@rm $(DESTDIR)/usr/include/sg_io_linux.h
	@rm $(DESTDIR)/usr/include/sg_lib.h
	@rm $(DESTDIR)/usr/include/sg_lib_data.h
	@rm $(DESTDIR)/usr/include/sg_linux_inc.h
	@rm $(DESTDIR)/usr/include/sg_pt.h
	@rm $(DESTDIR)/usr/include/sg_pt_win32.h
	@rm $(DESTDIR)/usr/lib32/sg_lib.o
	@rm $(DESTDIR)/usr/lib32/sg_io_linux.o
	@rm $(DESTDIR)/usr/lib32/sg_lib_data.o
	@rm $(DESTDIR)/usr/lib/sg_lib.o
	@rm $(DESTDIR)/usr/lib/sg_io_linux.o
	@rm $(DESTDIR)/usr/lib/sg_lib_data.o

reinstall: uninstall install

clean:
	@rm -f *.o *~ $(EXEC_DIR)/$(EXECS)
