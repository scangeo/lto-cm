**lto-cm**
=======

Read and write the text attribute of a Linear Tape Open (LTO) cartridge memory chip, also called Medium Access Memory (MAM).


**How to Generate Binaries**
============================

_Requires gcc_

- Download root directory of lto-cm
- Decompress and open location in terminal
- Type `make`

This should provide binaries for lto-cm

**If you want to install the utility on your system:**

- Type `make install`
